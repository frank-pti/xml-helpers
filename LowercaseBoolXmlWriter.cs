using System;
using System.Xml;
using System.IO;

namespace Frank.Helpers.Xml
{
    /// <summary>
    /// XML writer that prints bool values in lowercase.
    /// </summary>
    public class LowercaseBoolXmlWriter: XmlWriter
    {
        private XmlWriter xmlWriter;

        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Helpers.Xml.LowercaseBoolXmlWriter"/> class.
        /// </summary>
        /// <param name="stream">Stream.</param>
        /// <param name="settings">Settings.</param>
        public LowercaseBoolXmlWriter(Stream stream, XmlWriterSettings settings):
            this(XmlWriter.Create(stream, settings))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Helpers.Xml.LowercaseBoolXmlWriter"/> class.
        /// </summary>
        /// <param name="xmlWriter">Xml writer.</param>
        public LowercaseBoolXmlWriter (XmlWriter xmlWriter)
        {
            this.xmlWriter = xmlWriter;
        }

        #region implemented abstract members of System.Xml.XmlWriter
        /// <inheritdoc/>
        public override void Close ()
        {
            xmlWriter.Close();
        }

        /// <inheritdoc/>
        public override void Flush ()
        {
            xmlWriter.Flush();
        }

        /// <inheritdoc/>
        public override string LookupPrefix (string ns)
        {
            return xmlWriter.LookupPrefix(ns);
        }

        /// <inheritdoc/>
        public override void WriteBase64 (byte[] buffer, int index, int count)
        {
            xmlWriter.WriteBase64(buffer, index, count);
        }

        /// <inheritdoc/>
        public override void WriteCData (string text)
        {
            xmlWriter.WriteCData(text);
        }

        /// <inheritdoc/>
        public override void WriteCharEntity (char ch)
        {
            xmlWriter.WriteCharEntity(ch);
        }

        /// <inheritdoc/>
        public override void WriteChars (char[] buffer, int index, int count)
        {
            xmlWriter.WriteChars(buffer, index, count);
        }

        /// <inheritdoc/>
        public override void WriteComment (string text)
        {
            xmlWriter.WriteComment(text);
        }

        /// <inheritdoc/>
        public override void WriteDocType (string name, string pubid, string sysid, string subset)
        {
            xmlWriter.WriteDocType(name, pubid, sysid, subset);
        }

        /// <inheritdoc/>
        public override void WriteEndAttribute ()
        {
            xmlWriter.WriteEndAttribute();
        }

        /// <inheritdoc/>
        public override void WriteEndDocument ()
        {
            xmlWriter.WriteEndDocument();
        }

        /// <inheritdoc/>
        public override void WriteEndElement ()
        {
            xmlWriter.WriteEndElement();
        }

        /// <inheritdoc/>
        public override void WriteEntityRef (string name)
        {
            xmlWriter.WriteEntityRef(name);
        }

        /// <inheritdoc/>
        public override void WriteFullEndElement ()
        {
            xmlWriter.WriteFullEndElement();
        }

        /// <inheritdoc/>
        public override void WriteProcessingInstruction (string name, string text)
        {
            xmlWriter.WriteProcessingInstruction(name, text);
        }

        /// <inheritdoc/>
        public override void WriteRaw (string data)
        {
            xmlWriter.WriteRaw(data);
        }

        /// <inheritdoc/>
        public override void WriteRaw (char[] buffer, int index, int count)
        {
            xmlWriter.WriteRaw(buffer, index, count);
        }

        /// <inheritdoc/>
        public override void WriteStartAttribute (string prefix, string localName, string ns)
        {
            xmlWriter.WriteStartAttribute(prefix, localName, ns);
        }

        /// <inheritdoc/>
        public override void WriteStartDocument ()
        {
            xmlWriter.WriteStartDocument();
        }

        /// <inheritdoc/>
        public override void WriteStartDocument (bool standalone)
        {
            xmlWriter.WriteStartDocument(standalone);
        }

        /// <inheritdoc/>
        public override void WriteStartElement (string prefix, string localName, string ns)
        {
            xmlWriter.WriteStartElement(prefix, localName, ns);
        }

        /// <inheritdoc/>
        public override void WriteString (string text)
        {
            string lower = text.ToLowerInvariant();
            if (lower.Equals("false") || lower.Equals("true")) {
                text = lower;
            }
            xmlWriter.WriteString(text);
        }

        /// <inheritdoc/>
        public override void WriteSurrogateCharEntity (char lowChar, char highChar)
        {
            xmlWriter.WriteSurrogateCharEntity(lowChar, highChar);
        }

        /// <inheritdoc/>
        public override void WriteWhitespace (string ws)
        {
            xmlWriter.WriteWhitespace(ws);
        }

        /// <inheritdoc/>
        public override WriteState WriteState {
            get {
                return xmlWriter.WriteState;
            }
        }
        #endregion
    }
}

