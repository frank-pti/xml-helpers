using System;
using System.Xml;
using System.IO;
using System.Collections.Generic;

namespace Frank.Helpers.Xml
{
    /// <summary>
    /// XML wirter that stripps the namespace.
    /// </summary>
    public class NamespaceStrippingXmlWriter: XmlWriter
    {
        private XmlWriter xmlWriter;
        private Stack<bool> attributeStack;
        private bool stripNextString;
        private string nameSpaceUrl;
        private string nameSpacePrefix;

        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Helpers.Xml.NamespaceStrippingXmlWriter"/> class.
        /// </summary>
        /// <param name="stream">Stream.</param>
        /// <param name="settings">Settings.</param>
        /// <param name="nameSpacePrefix">Name space prefix.</param>
        /// <param name="nameSpaceUrl">Name space URL.</param>
        public NamespaceStrippingXmlWriter (Stream stream, XmlWriterSettings settings, string nameSpacePrefix, string nameSpaceUrl):
            this(XmlWriter.Create(stream, settings), nameSpacePrefix, nameSpaceUrl)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Helpers.Xml.NamespaceStrippingXmlWriter"/> class.
        /// </summary>
        /// <param name="xmlWriter">Xml writer.</param>
        /// <param name="nameSpacePrefix">Name space prefix.</param>
        /// <param name="nameSpaceUrl">Name space URL.</param>
        public NamespaceStrippingXmlWriter (XmlWriter xmlWriter, string nameSpacePrefix, string nameSpaceUrl)
        {
            this.xmlWriter = xmlWriter;
            this.attributeStack = new Stack<bool>();
            this.stripNextString = false;
            this.nameSpacePrefix = nameSpacePrefix;
            this.nameSpaceUrl = nameSpaceUrl;
        }

        #region implemented abstract members of System.Xml.XmlWriter
        /// <inheritdoc/>
        public override void Close ()
        {
            xmlWriter.Close();
        }

        /// <inheritdoc/>
        public override void Flush ()
        {
            xmlWriter.Flush();
        }

        /// <inheritdoc/>
        public override string LookupPrefix (string ns)
        {
            return xmlWriter.LookupPrefix(ns);
        }

        /// <inheritdoc/>
        public override void WriteBase64 (byte[] buffer, int index, int count)
        {
            xmlWriter.WriteBase64(buffer, index, count);
        }

        /// <inheritdoc/>
        public override void WriteCData (string text)
        {
            xmlWriter.WriteCData(text);
        }

        /// <inheritdoc/>
        public override void WriteCharEntity (char ch)
        {
            xmlWriter.WriteCharEntity(ch);
        }

        /// <inheritdoc/>
        public override void WriteChars (char[] buffer, int index, int count)
        {
            xmlWriter.WriteChars(buffer, index, count);
        }

        /// <inheritdoc/>
        public override void WriteComment (string text)
        {
            xmlWriter.WriteComment(text);
        }

        /// <inheritdoc/>
        public override void WriteDocType (string name, string pubid, string sysid, string subset)
        {
            xmlWriter.WriteDocType(name, pubid, sysid, subset);
        }

        /// <inheritdoc/>
        public override void WriteEndAttribute ()
        {
            if (attributeStack.Pop()) {
                xmlWriter.WriteEndAttribute();
            }
        }

        /// <inheritdoc/>
        public override void WriteEndDocument ()
        {
            xmlWriter.WriteEndDocument();
        }

        /// <inheritdoc/>
        public override void WriteEndElement ()
        {
            xmlWriter.WriteEndElement();
        }

        /// <inheritdoc/>
        public override void WriteEntityRef (string name)
        {
            xmlWriter.WriteEntityRef(name);
        }

        /// <inheritdoc/>
        public override void WriteFullEndElement ()
        {
            xmlWriter.WriteFullEndElement();
        }

        /// <inheritdoc/>
        public override void WriteProcessingInstruction (string name, string text)
        {
            xmlWriter.WriteProcessingInstruction(name, text);
        }

        /// <inheritdoc/>
        public override void WriteRaw (string data)
        {
            xmlWriter.WriteRaw(data);
        }

        /// <inheritdoc/>
        public override void WriteRaw (char[] buffer, int index, int count)
        {
            xmlWriter.WriteRaw(buffer, index, count);
        }

        /// <inheritdoc/>
        public override void WriteStartAttribute (string prefix, string localName, string ns)
        {
            bool write = (ns != this.nameSpaceUrl) && (localName != this.nameSpacePrefix || prefix != "xmlns");
            if (write) {
                xmlWriter.WriteStartAttribute(prefix, localName, ns);
            } else {
                stripNextString = true;
            }
            attributeStack.Push(write);
        }

        /// <inheritdoc/>
        public override void WriteStartDocument ()
        {
            xmlWriter.WriteStartDocument();
        }

        /// <inheritdoc/>
        public override void WriteStartDocument (bool standalone)
        {
            xmlWriter.WriteStartDocument(standalone);
        }

        /// <inheritdoc/>
        public override void WriteStartElement (string prefix, string localName, string ns)
        {
            xmlWriter.WriteStartElement(prefix, localName, ns);
        }

        /// <inheritdoc/>
        public override void WriteString (string text)
        {
            string lower = text.ToLowerInvariant();
            if (lower.Equals("false") || lower.Equals("true")) {
                text = lower;
            }
            if (!stripNextString) {
                xmlWriter.WriteString(text);
            }
            stripNextString = false;
        }

        /// <inheritdoc/>
        public override void WriteSurrogateCharEntity (char lowChar, char highChar)
        {
            xmlWriter.WriteSurrogateCharEntity(lowChar, highChar);
        }

        /// <inheritdoc/>
        public override void WriteWhitespace (string ws)
        {
            xmlWriter.WriteWhitespace(ws);
        }

        /// <inheritdoc/>
        public override WriteState WriteState {
            get {
                return xmlWriter.WriteState;
            }
        }
        #endregion
    }
}

